import grovepi
import math

sensor = 4  # Utiliser le port 4

blue = 0    # sensor normal
white = 1   # sensor pro

try:
    [temp,humidity] = grovepi.dht(sensor,white)  
    if math.isnan(temp) == False and math.isnan(humidity) == False:
        f = open("data.txt", "a+")
        f.write("%.02f;%.02f\n"%(temp, humidity)) #append des données au fichier
	    f.close()
        print("temp = %.02f C humidity =%.02f%%"%(temp, humidity))

except IOError:
    print ("Error")
