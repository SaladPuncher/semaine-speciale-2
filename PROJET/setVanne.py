import grovepi
import math

sensor = 4
led = 3

while True:
    try:
        [temp,humidity] = grovepi.dht(sensor,1)  
        if math.isnan(temp) == False and math.isnan(humidity) == False:
            if humidity < 75:
                digitalWrite(led,1)
            else :
                digitalWrite(led,0)
    
    except IOError:
        print ("Error")
        
    except KeyboardInterrupt:
        digitalWrite(led,0)
        break;
