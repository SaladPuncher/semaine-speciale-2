import time
import grovepi
from grovepi import *
import math
import subprocess


sensorAir = 5
reset = 6
sensorSoil = 7
#air sec :	environ 0
#terre seche :	entre 0 et 300
#terre humide : entre 300 et 700
#immerge :	entre 700 et 950
led = 8

pinMode(led,"OUTPUT")

while True:
    time.sleep(.100)
    fC = open("current.txt", "w")
    numLines = 0
    with open("history.txt", "a+") as fH:
        for line in fH:
             numLines+=1
    fH = open("history.txt", "a")
    try:
	rb = grovepi.digitalRead(reset)
	if rb == 1:
	    cmd = "reboot"
	    process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
            output, error = process.communicate()
        [temp,humidity] = grovepi.dht(sensorAir,1)
	soilHumi = grovepi.analogRead(sensorSoil)
        if math.isnan(temp) == False and math.isnan(humidity) == False:
	    fC.write("%.02f %.02f %.02f\n"%(temp,humidity,soilHumi))
	    if numLines>100 :
		cmd = "sed -i 1d history.txt"
		process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
		output, error = process.communicate()
	    fH.write("%.02f %.02f %.02f\n"%(temp,humidity,soilHumi))
	    print(numLines)
	    fH.close()
            if int(soilHumi) < 580:
                grovepi.digitalWrite(led,1) #allumer les vannes
		print("need soil eau")
		print(soilHumi)
            else :
                grovepi.digitalWrite(led,0) #eteindre les vannes
		print("ok")
    
    except IOError:
        print ("Error")
        
    except KeyboardInterrupt:
        digitalWrite(led,0)
	fH.close()
	fC.close()
        break;
