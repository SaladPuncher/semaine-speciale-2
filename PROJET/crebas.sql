/*==============================================================*/
/* Nom de SGBD :  Sybase SQL Anywhere 12                        */
/* Date de création :  20/06/2019 10:01:12                      */
/*==============================================================*/

drop table if exists Plantes;

/*==============================================================*/
/* Table : PLANTES                                              */
/*==============================================================*/
create table PLANTES 
(
   ESPECE               varchar(49) unique                  not null,
   HUMIDITE             float                         not null,
   TEMPERATURE          float                         not null,
   QUANTITE_EAU         float                         not null,
   constraint PK_PLANTES primary key (ESPECE)
);