/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package irrigation;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.border.Border;

/**
 * C'est la classe qui permet d'afficher l'application
 * @author Virgile Gabrieli
 */


public class Screen extends JFrame implements ActionListener {
    ///Attributs
    private ImagePanel pan;
    private GridBagConstraints cons;
    private ArrayList<String> tabIcon;
    //divers
    private ArrayList<Plante> dicoPlante;
    private Plantes plantes;//classe plantes contenant listeplante
    private FocusMenu focus;//Détermine le focus sur le menu actuel pour séparer l'activation des boutons
    private JButton retour,ajouter,supprimer;
    private Plante focusPlante;//determine le focus sur laplante modifier; =null si feature non utilisée
    private boolean check;
    //private int ite;
    //initAccueil
    private JButton mesPlantes,ajouterPlantes,gestionCapteurs,gestionEspece,parametre;
    //initPlantes
    private JButton[] tabB;//tableau de bouton menant aux detail des plantes
    private JButton[] tabSupp;//tableau de bouton menant aux suppressions des plantes
    private JButton[] tabModif;//tableau de bouton menant aux modifications des plantes
    //initGestion
    private JLabel valueHum,valueLum,valueTemp;//Valeures modifiables par appel
    private JButton STOP,graph;
    //initAjouterPlante
    private JTextField texte,texth,textl,textt,textqe;
    private JComboBox menuEspece;
    //initGraphs
    private JRadioButton rhumi,rlumi,rtemp;
    private ButtonGroup grp;
    //Rajouter composant (graphs)
    //initBD
    private JButton explo,add,supp,modif;
    
    //constructeur
    /**
     * Affichage de la page d'accueil.
     * @see InitAcceuil();
     * @throws ClassNotFoundException si on ne trouve pas la classe (erreur de lecture/d'ecriture)
     * @throws SQLException  Si on ne trouve pas la base de donnée (erreur de lecture/d'ecriture)
     */
    Screen() throws ClassNotFoundException, SQLException{ 
        //DECLARATION jpan ici avec gif chargement
        LoadDico();
        IconInitialisation();
        this.setUndecorated(true);
        this.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        pan=new ImagePanel(new ImageIcon(tabIcon.get(0)).getImage());//Creation panel
        pan.setLayout(new GridBagLayout());//creation layout
        pan.setBackground(Color.white);//WTF
        cons = new GridBagConstraints(); 
        valueHum= new JLabel("Cond. actuelle");valueLum= new JLabel("-");valueTemp= new JLabel("Cond. actuelle");
        //this.setIconImage(new ImageIcon("Z:\\Bureau\\PROJET\\g1760.png").getImage());
        this.setPreferredSize(new Dimension(800,480));
        this.setTitle("Gestion de l'irrigation");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.InitAcceuil();
        this.setVisible(true);
        //LoadDico();
        check=false;
    }
    
    /**
     * Fonction innitialisant toutes les images.
     */
    public void IconInitialisation(){
        tabIcon=new ArrayList<>();
        tabIcon.add("/home/pi/Bureau/B/imageFond.png");
        tabIcon.add("/home/pi/Bureau/B/MesPlantesD.png");
        tabIcon.add("/home/pi/Bureau/B/MesPlantesA.png");
        tabIcon.add("/home/pi/Bureau/B/AjouterPlanteD.png");
        tabIcon.add("/home/pi/Bureau/B/AjouterPlanteA.png");
        tabIcon.add("/home/pi/Bureau/B/GererIrrD.png");
        tabIcon.add("/home/pi/Bureau/B/GererIrrA.png");
        tabIcon.add("/home/pi/Bureau/B/GererBDD.png");
        tabIcon.add("/home/pi/Bureau/B/GererBDA.png");
        tabIcon.add("/home/pi/Bureau/B/ParametreD.png");
        tabIcon.add("/home/pi/Bureau/B/ParametreA.png");
        tabIcon.add("/home/pi/Bureau/B/g1760.png");//PLACER JUSQU ICI (11)
        tabIcon.add("/home/pi/Bureau/B/RetourAcceuilbisD.png");
        tabIcon.add("/home/pi/Bureau/B/RetourAcceuilbisA.png");
        tabIcon.add("/home/pi/Bureau/B/AjouterPlanteD.png");
        tabIcon.add("/home/pi/Bureau/B/AjouterPlanteA.png");
        tabIcon.add("/home/pi/Bureau/B/DetailsD.png");
        tabIcon.add("/home/pi/Bureau/B/DetailsA.png");
        tabIcon.add("/home/pi/Bureau/B/ModifierD.png");
        tabIcon.add("/home/pi/Bureau/B/ModifierA.png");
        tabIcon.add("/home/pi/Bureau/B/SupprimerD.png");
        tabIcon.add("/home/pi/Bureau/B/SupprimerA.png");
        tabIcon.add("/home/pi/Bureau/B/RetourAcceuilD.png");
        tabIcon.add("/home/pi/Bureau/B/RetourAcceuilA.png");
        tabIcon.add("");
        tabIcon.add("");
        
    }
        
    /**
     * Initialisation de la page d'acceuil (création des boutons, mise en place des images/logo, etc).
     */
    public void InitAcceuil(){
        focus=FocusMenu.Acceuil;
        pan.removeAll();
        ///NEW JBUTTON///
        mesPlantes= new JButton(new ImageIcon(tabIcon.get(1)));mesPlantes.setBorder(null);mesPlantes.setPressedIcon(new ImageIcon(tabIcon.get(2)));
        ajouterPlantes = new JButton(new ImageIcon(tabIcon.get(3)));ajouterPlantes.setBorder(null);ajouterPlantes.setPressedIcon(new ImageIcon(tabIcon.get(4)));
        gestionCapteurs = new JButton(new ImageIcon(tabIcon.get(5)));gestionCapteurs.setBorder(null);gestionCapteurs.setPressedIcon(new ImageIcon(tabIcon.get(6)));
        gestionEspece=new JButton(new ImageIcon(tabIcon.get(7)));gestionEspece.setBorder(null);gestionEspece.setPressedIcon(new ImageIcon(tabIcon.get(8)));
        parametre=new JButton(new ImageIcon(tabIcon.get(9)));parametre.setBorder(null);parametre.setPressedIcon(new ImageIcon(tabIcon.get(10)));
        
       // JLabel im=new JLabel(new ImageIcon("Z:\\safe_image.gif"));
        
        ////////PLACMENT DES COMPOSANTS////////// 
       // cons.gridx=0;cons.gridy=0;cons.gridheight=7;cons.gridwidth=1;
       // pan.add(im,cons);
        cons.fill=GridBagConstraints.BOTH;
        cons.gridx=0;cons.gridy=0;cons.gridwidth=1;cons.gridheight=1;
        cons.insets=new Insets(0,0,20,0);
        pan.add(new JLabel(new ImageIcon(tabIcon.get(11))),cons);
        cons.gridy++;
        cons.insets=new Insets(6,4,6,4);
        pan.add(mesPlantes,cons);
        cons.gridy++;
        pan.add(ajouterPlantes,cons);
        cons.gridy++;
        pan.add(gestionCapteurs,cons);
        cons.gridy++;
        pan.add(gestionEspece,cons);
        cons.gridy++;
        cons.insets=new Insets(25,4,6,4);
        pan.add(parametre,cons);
        cons.insets=new Insets(6,4,6,4);
        this.setContentPane(pan);
        this.pack();
        
        mesPlantes.addActionListener(this);
        ajouterPlantes.addActionListener(this);
        gestionCapteurs.addActionListener(this);
        gestionEspece.addActionListener(this);
        parametre.addActionListener(this);
    }
   
    /**
     * Initialisation et affichage de la page menu de plante.
     * @throws FileNotFoundException si on ne trouve pas le fichier
     */
    public void InitPlantes() throws FileNotFoundException{
        save();
        focus=FocusMenu.MesPlantes;
        pan.removeAll();
        retour= new JButton(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilbisD.png"));retour.setBorder(null);retour.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilbisA.png"));
        Border border = BorderFactory.createLineBorder(Color.decode("#ffccaa"), 1);
        String compteur="Plante : "+plantes.getListePlante().size()+"/"+plantes.getPlantesMax();
        JLabel cptPlante= new JLabel(compteur);cptPlante.setBorder(border);cptPlante.setBackground(Color.decode("#552200"));cptPlante.setOpaque(true);cptPlante.setForeground(Color.white);cptPlante.setFont(new Font("Lobster",Font.PLAIN,18));cptPlante.setHorizontalAlignment(JLabel.CENTER);
        ajouterPlantes = new JButton(new ImageIcon("/home/pi/Bureau/B/AjouterPlanteD.png"));ajouterPlantes.setBorder(null);ajouterPlantes.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/AjouterPlanteA.png"));
        tabB= new JButton[plantes.getListePlante().size()];
        tabSupp=new JButton[plantes.getListePlante().size()];
        tabModif=new JButton[plantes.getListePlante().size()];

        ////////PLACMENT DES COMPOSANTS//////////
        cons.fill=GridBagConstraints.BOTH;
        cons.gridwidth=3;cons.gridx=0;cons.gridy=0;
        pan.add(cptPlante,cons);
        cons.gridwidth=3;cons.gridx=3; cons.gridy=0;
        pan.add(ajouterPlantes,cons);
        for(int i=0;i<plantes.getListePlante().size();i++){
            cons.gridx=0;cons.gridy++;cons.gridwidth=3;
            JLabel plante=new JLabel(" "+(i+1)+". Espèce : "+plantes.getListePlante().get(i).getEspece()+" ");
            plante.setBorder(border);plante.setBackground(Color.decode("#552200"));plante.setOpaque(true);plante.setForeground(Color.white);plante.setFont(new Font("Lobster",Font.PLAIN,18));
            pan.add(plante,cons);
            cons.gridwidth=1;cons.gridx=3;
            tabB[i] = new JButton(new ImageIcon("/home/pi/Bureau/B/DetailsD.png"));tabB[i].setBorder(null);tabB[i].setPressedIcon(new ImageIcon("/home/pi/Bureau/B/DetailsA.png"));
            tabB[i].addActionListener(this);
            pan.add(tabB[i],cons);
            cons.gridx=4;
            tabModif[i] = new JButton(new ImageIcon("/home/pi/Bureau/B/ModifierD.png"));tabModif[i].setBorder(null);tabModif[i].setPressedIcon(new ImageIcon("/home/pi/Bureau/B/ModifierA.png"));
            tabModif[i].addActionListener(this);
            pan.add(tabModif[i],cons);
            cons.gridx=5;
            tabSupp[i] = new JButton(new ImageIcon("/home/pi/Bureau/B/SupprimerD.png"));tabSupp[i].setBorder(null);tabSupp[i].setPressedIcon(new ImageIcon("/home/pi/Bureau/B/SupprimerA.png"));
            tabSupp[i].addActionListener(this);
            pan.add(tabSupp[i],cons);
        }
        cons.gridwidth=3;cons.gridx=3;cons.gridy++;
        retour = new JButton(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilD.png"));retour.setBorder(null);retour.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilA.png"));
        pan.add(retour,cons);
        
        this.setContentPane(pan);
        this.pack();
        
        retour.addActionListener(this);
        ajouterPlantes.addActionListener(this);
    }
    
    /**
     * Initialisation et affichage de la page gestion des plantes
     */
    public void InitGestion(){
        focus=FocusMenu.Gestion;
        pan.removeAll();
        retour= new JButton(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilD.png"));retour.setBorder(null);retour.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilA.png"));
        Border border = BorderFactory.createLineBorder(Color.decode("#ffccaa"), 1);
        graph= new JButton(new ImageIcon("/home/pi/Bureau/B/AffichergraphD.png"));graph.setBorder(null);graph.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/AffichergraphA.png"));
        //ARRETURGENT
        STOP= new JButton(new ImageIcon("/home/pi/Bureau/B/ARRETURGENTD.png"));STOP.setBorder(null);STOP.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/ARRETURGENTA.png"));

        ////////PLACMENT DES COMPOSANTS//////////
        cons.fill=GridBagConstraints.BOTH;
        cons.gridwidth=2;cons.gridx=0;cons.gridy=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabHumidité.png")),cons);
        cons.gridx=2;
        valueHum.setBorder(border);valueHum.setBackground(Color.decode("#552200"));valueHum.setOpaque(true);valueHum.setForeground(Color.white);valueHum.setFont(new Font("Lobster",Font.PLAIN,18));valueHum.setHorizontalAlignment(JLabel.CENTER);
        pan.add(valueHum,cons);
        cons.gridx=0;cons.gridy++;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabLuminosité.png")),cons);
        cons.gridx=2;
        valueLum.setBorder(border);valueLum.setBackground(Color.decode("#552200"));valueLum.setOpaque(true);valueLum.setForeground(Color.white);valueLum.setFont(new Font("Lobster",Font.PLAIN,18));valueLum.setHorizontalAlignment(JLabel.CENTER);
        pan.add(valueLum,cons);
        cons.gridx=0;cons.gridy++;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabTempérature.png")),cons);
        cons.gridx=2;
        valueTemp.setBorder(border);valueTemp.setBackground(Color.decode("#552200"));valueTemp.setOpaque(true);valueTemp.setForeground(Color.white);valueTemp.setFont(new Font("Lobster",Font.PLAIN,18));valueTemp.setHorizontalAlignment(JLabel.CENTER);
        pan.add(valueTemp,cons);
        cons.gridwidth=4;cons.gridx=0;cons.gridy++;
        graph.setBackground(Color.lightGray);
        pan.add(graph,cons);
        cons.gridy++;
        STOP.setBackground(Color.orange);
        pan.add(STOP,cons);
        cons.gridy++;
        retour.setBackground(Color.red);
        pan.add(retour,cons);
        
        this.setContentPane(pan);
        this.pack();
        
        graph.addActionListener(this);
        retour.addActionListener(this);
        STOP.addActionListener(this);
    }
    
    /**
     * Initialisation et affichage de la page ajout de plante
     */
    public void InitAjouterPlante(){
        focus=FocusMenu.AjouterPlante;
        pan.removeAll();
        retour= new JButton(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilD.png"));retour.setBorder(null);retour.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilA.png"));
        Border border = BorderFactory.createLineBorder(Color.black, 1);
        JLabel type;
        ajouter= new JButton(new ImageIcon("/home/pi/Bureau/B/AjouterEspeceD.png"));ajouter.setBorder(null);ajouter.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/AjouterEspeceA.png"));
        
        menuEspece =new JComboBox();// A REMPLACER PAR UNE LECTURE DE FICHIER TEXTE
        for(int i=0;i<dicoPlante.size();i++){
            menuEspece.addItem(dicoPlante.get(i).getEspece());
        }
        ////////PLACMENT DES COMPOSANTS//////////
        cons.fill=GridBagConstraints.BOTH;

        cons.gridwidth=2;
        cons.gridx=0;
        cons.gridy=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/Espece.png")),cons);
        cons.gridwidth=1;
        cons.gridx=0;
        cons.gridy++;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabEspece.png")),cons);
        cons.gridx=1;
        pan.add(menuEspece,cons);

        cons.fill=2;
        cons.gridwidth=2;
        cons.gridx=0;
        cons.gridy++;
        ajouter.setBackground(Color.green);
        pan.add(ajouter,cons);
        cons.gridy++;
        retour.setBackground(Color.red);
        pan.add(retour,cons);
        
        this.setContentPane(pan);
        this.pack();
        
        ajouter.addActionListener(this);
        retour.addActionListener(this);

    }        
    
   /**
    * Initialisation et affichage de la page parametre.
    */
    public void InitParametre(){
        focus=FocusMenu.Parametre;
        pan.removeAll();
        retour= new JButton(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilD.png"));retour.setBorder(null);retour.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilA.png"));
        cons.gridx=cons.gridy=0;
        pan.add(retour,cons);
        retour.addActionListener(this);
        this.setContentPane(pan);
        this.pack();
    }
    
    /**
     * Initialisation et affichage de la page des graphiques.
     */
    public void InitGraphs(){
        focus=FocusMenu.Graphiques;
        pan.removeAll();
        retour= new JButton(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilD.png"));retour.setBorder(null);retour.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilA.png"));
        rhumi=new JRadioButton("Humidité");
        rlumi=new JRadioButton("Luminosité");
        rtemp=new JRadioButton("Temperature");
        grp=new ButtonGroup();
        grp.add(rhumi);
        grp.add(rlumi);
        grp.add(rtemp);
        rhumi.setSelected(true);
        
        cons.gridwidth=1;
        cons.gridx=0;
        cons.gridy=0;
        pan.add(rhumi,cons);
        cons.gridx++;
        pan.add(rlumi,cons);
        cons.gridx++;
        pan.add(rtemp,cons);
        
        cons.gridwidth=3;
        cons.gridx=0;
        cons.gridy++;
        retour.setBackground(Color.red);
        pan.add(retour,cons);
        
        this.setContentPane(pan);
        this.pack();
        
        rhumi.addActionListener(this);
        rlumi.addActionListener(this);
        rtemp.addActionListener(this);
        retour.addActionListener(this);
    }
    
    /**
     * Initialisation et affichage de la page modifier une plante.
     */
    public void InitModif(){
        focus=FocusMenu.Modif;
        pan.removeAll();
        retour= new JButton(new ImageIcon("/home/pi/Bureau/B/RetourD.png"));retour.setBorder(null);retour.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/RetourA.png"));
        ajouter= new JButton(new ImageIcon("/home/pi/Bureau/B/ModPlanteD.png"));ajouter.setBorder(null);ajouter.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/ModPlanteA.png"));
        
        texte=new JTextField(focusPlante.getEspece());
        texth=new JTextField(""+focusPlante.getConditionsOptimale().getHumidite());
        textl=new JTextField(""+focusPlante.getConditionsOptimale().getLuminosite());
        textt=new JTextField(""+focusPlante.getConditionsOptimale().getTemperature());
        textqe=new JTextField(""+focusPlante.getConditionsOptimale().getQuantiteEau());
        texte.setColumns(5);

        ////////PLACMENT DES COMPOSANTS//////////
        cons.fill=GridBagConstraints.BOTH;
        cons.gridwidth=1;cons.gridx=0;cons.gridy=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabEspece.png")),cons);
        cons.gridx=1;
        pan.add(texte,cons);
        cons.gridx=0;cons.gridy++;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabCondition.png")),cons);
        cons.gridy++;cons.gridx=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabLuminosité.png")),cons);
        cons.gridx=1;
        pan.add(textl,cons);
        cons.gridy++;cons.gridx=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabTempérature.png")),cons);
        cons.gridx=1;
        pan.add(textt,cons);
        cons.gridy++;cons.gridx=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabHumidité.png")),cons);
        cons.gridx=1;
        pan.add(texth,cons);
        cons.gridy++;cons.gridx=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabQuantitéDeau.png")),cons);
        cons.gridx=1;
        pan.add(textqe,cons);
        cons.gridwidth=2;cons.gridx=0;cons.gridy++;
        pan.add(ajouter,cons);
        cons.gridy++;
        pan.add(retour,cons);
        
        this.setContentPane(pan);
        this.pack();
        
        ajouter.addActionListener(this);
        retour.addActionListener(this);
    }
    
    /**
     * Initialisation et affichage de la page Index BD, qui est un menu vers les differentes fonctions utilisant la base de données
     */
    public void InitIndexBD(){
        focus=FocusMenu.IndexBD;
        pan.removeAll();
        retour= new JButton(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilD.png"));retour.setBorder(null);retour.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/RetourAcceuilA.png"));
        ///NEW JBUTTON///
        explo= new JButton(new ImageIcon("/home/pi/Bureau/B/ExploBDD.png"));explo.setBorder(null);explo.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/ExploBDA.png"));
        add= new JButton(new ImageIcon("/home/pi/Bureau/B/AjouterBDD.png"));add.setBorder(null);add.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/AjouterBDA.png"));
        supp= new JButton(new ImageIcon("/home/pi/Bureau/B/SupprimerBDD.png"));supp.setBorder(null);supp.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/SupprimerBDA.png"));
        modif= new JButton(new ImageIcon("/home/pi/Bureau/B/ModifierBDD.png"));modif.setBorder(null);modif.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/ModifierBDA.png"));

        cons.fill=GridBagConstraints.BOTH;
        cons.gridx=0;cons.gridy=0;
        cons.insets=new Insets(25,4,6,4);
        pan.add(explo,cons);
        cons.insets=new Insets(6,4,6,4);
        cons.gridy++;
        pan.add(add,cons);
        cons.gridy++;
        pan.add(supp,cons);
        cons.gridy++;
        pan.add(modif,cons);
        cons.gridy++;
        cons.insets=new Insets(25,4,6,4);
        pan.add(retour,cons);
        cons.insets=new Insets(6,4,6,4);
        
        this.setContentPane(pan);
        this.pack();
        retour.addActionListener(this);
        add.addActionListener(this);
        supp.addActionListener(this);
        modif.addActionListener(this);
        explo.addActionListener(this);
    }
    
    /**
     * Initialisation et affichage de la page liste de plante dans la Base de données
     */
    public void InitExploBD(){
        focus=FocusMenu.ExploBD;
        pan.removeAll();
        Border border = BorderFactory.createLineBorder(Color.decode("#ffccaa"), 1);
        retour= new JButton(new ImageIcon("/home/pi/Bureau/B/RetourD.png"));retour.setBorder(null);retour.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/RetourA.png"));
        JLabel type;
        if(!check){
            menuEspece=new JComboBox();
            for(int i=0;i<dicoPlante.size();i++){
                 menuEspece.addItem(dicoPlante.get(i).getEspece());
            }
        }
        
        cons.fill=GridBagConstraints.BOTH;
        cons.gridx=0;cons.gridy=0;cons.gridwidth=2;
        pan.add(menuEspece,cons);
        if(check){
            cons.gridwidth=1;cons.gridy++;
            pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabTempérature.png")),cons);
            cons.gridx++;
            type=new JLabel(""+dicoPlante.get(menuEspece.getSelectedIndex()).getConditionsOptimale().getTemperature());
            type.setBorder(border);type.setBackground(Color.decode("#552200"));type.setOpaque(true);type.setForeground(Color.white);type.setFont(new Font("Lobster",Font.PLAIN,18));type.setHorizontalAlignment(JLabel.CENTER);
            pan.add(type,cons);
            cons.gridx=0;cons.gridy++;
            pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabLuminosité.png")),cons);
            cons.gridx++;
            type=new JLabel(""+dicoPlante.get(menuEspece.getSelectedIndex()).getConditionsOptimale().getLuminosite());
            type.setBorder(border);type.setBackground(Color.decode("#552200"));type.setOpaque(true);type.setForeground(Color.white);type.setFont(new Font("Lobster",Font.PLAIN,18));type.setHorizontalAlignment(JLabel.CENTER);
            pan.add(type,cons);
            cons.gridx=0;cons.gridy++;
            pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabHumidité.png")),cons);
            cons.gridx++;
            type=new JLabel(""+dicoPlante.get(menuEspece.getSelectedIndex()).getConditionsOptimale().getHumidite());
            type.setBorder(border);type.setBackground(Color.decode("#552200"));type.setOpaque(true);type.setForeground(Color.white);type.setFont(new Font("Lobster",Font.PLAIN,18));type.setHorizontalAlignment(JLabel.CENTER);
            pan.add(type,cons);
            cons.gridx=0;cons.gridy++;
            pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabQuantitéDeau.png")),cons);
            cons.gridx++;
            type=new JLabel(""+dicoPlante.get(menuEspece.getSelectedIndex()).getConditionsOptimale().getQuantiteEau());
            type.setBorder(border);type.setBackground(Color.decode("#552200"));type.setOpaque(true);type.setForeground(Color.white);type.setFont(new Font("Lobster",Font.PLAIN,18));type.setHorizontalAlignment(JLabel.CENTER);
            pan.add(type,cons);
        }
        cons.gridwidth=2;
        cons.gridx=0;cons.gridy++;
        pan.add(retour,cons);
        
        this.setContentPane(pan);
        this.pack();
        menuEspece.addActionListener(this);
        retour.addActionListener(this);
    }
    
    /**
     * Initialisation et affichage de la page suppression d'une plante dans la base de données
     */
    public void InitsuppBD(){
        focus=FocusMenu.SuppBD;
        pan.removeAll();
        retour= new JButton(new ImageIcon("/home/pi/Bureau/B/RetourD.png"));retour.setBorder(null);retour.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/RetourA.png"));
        Border border = BorderFactory.createLineBorder(Color.black, 1);
        JLabel type;
        supprimer= new JButton(new ImageIcon("/home/pi/Bureau/B/SupprimerBDD.png"));supprimer.setBorder(null);supprimer.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/SupprimerBDA.png"));
        menuEspece =new JComboBox();// A REMPLACER PAR UNE LECTURE DE FICHIER TEXTE
        for(int i=0;i<dicoPlante.size();i++){
            menuEspece.addItem(dicoPlante.get(i).getEspece());
        }
        ////////PLACMENT DES COMPOSANTS//////////
        cons.fill=GridBagConstraints.BOTH;
        cons.gridwidth=2;cons.gridx=0;cons.gridy=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabSuppEspece.png")),cons);
        //LabSuppEspece
        cons.gridwidth=1;
        cons.gridx=0;cons.gridy++;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabEspece.png")),cons);
        cons.gridx=1;
        pan.add(menuEspece,cons);
        cons.gridwidth=2;cons.gridx=0;cons.gridy++;
        supprimer.setBackground(Color.green);
        pan.add(supprimer,cons);
        cons.gridy++;
        pan.add(retour,cons);
        
        this.setContentPane(pan);
        this.pack();
        
        supprimer.addActionListener(this);
        retour.addActionListener(this);
    }
    
    /**
     * Initialisation et affichage de la page ajout d'une plante dans la base de données
     */
    public void InitaddBD(){
        focus=FocusMenu.AddBD;
        pan.removeAll();
        retour= new JButton(new ImageIcon("/home/pi/Bureau/B/RetourD.png"));retour.setBorder(null);retour.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/RetourA.png"));
        Border border = BorderFactory.createLineBorder(Color.black, 1);
        JLabel type = new JLabel("Liste des espèces enregistrées :");type.setBorder(border);
        texth=new JTextField("");
        textl=new JTextField("");
        textt=new JTextField("");
        textqe=new JTextField("");
        texte=new JTextField("");
        texte.setColumns(10);
        ajouter= new JButton(new ImageIcon("/home/pi/Bureau/B/AjouterBDD.png"));ajouter.setBorder(null);ajouter.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/AjouterBDA.png"));
        
        cons.fill=GridBagConstraints.BOTH;
        cons.gridx=0;cons.gridy=0;cons.gridy++;cons.gridwidth=2;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/Espece.png")),cons);
        cons.gridwidth=1;cons.gridx=0;cons.gridy++;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabEspece.png")),cons);
        cons.gridx=1;
        pan.add(texte,cons);
        cons.gridy++;cons.gridx=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabLuminosité.png")),cons);
        cons.gridx=1;
        pan.add(textl,cons);
        cons.gridy++;cons.gridx=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabTempérature.png")),cons);
        cons.gridx=1;
        pan.add(textt,cons);
        cons.gridy++;cons.gridx=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabHumidité.png")),cons);
        cons.gridx=1;
        pan.add(texth,cons);
        cons.gridy++;cons.gridx=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabQuantitéDeau.png")),cons);
        cons.gridx=1;
        pan.add(textqe,cons);
        cons.gridwidth=2;cons.gridx=0;cons.gridy++;
        pan.add(ajouter,cons);
        cons.gridy++;
        pan.add(retour,cons);
        
        this.setContentPane(pan);
        this.pack();
        retour.addActionListener(this);
        ajouter.addActionListener(this);
    }
    
    /**
     * Initialisation et affichage de la page modification d'une plante dans la base de données
     */
    public void InitmodifBD(){
        focus=FocusMenu.ModifBD;
        pan.removeAll();
        retour= new JButton(new ImageIcon("/home/pi/Bureau/B/RetourD.png"));retour.setBorder(null);retour.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/RetourA.png"));
        Border border = BorderFactory.createLineBorder(Color.black, 1);

        if(!check){
            ajouter= new JButton(new ImageIcon("/home/pi/Bureau/B/SauvegardermodifD.png"));ajouter.setBorder(null);ajouter.setPressedIcon(new ImageIcon("/home/pi/Bureau/B/SauvegardermodifA.png"));
            menuEspece=new JComboBox();
            for(int i=0;i<dicoPlante.size();i++){
                 menuEspece.addItem(dicoPlante.get(i).getEspece());
            }
        }
        texth=new JTextField(""+dicoPlante.get(menuEspece.getSelectedIndex()).getConditionsOptimale().getHumidite());
        textl=new JTextField(""+dicoPlante.get(menuEspece.getSelectedIndex()).getConditionsOptimale().getLuminosite());
        textt=new JTextField(""+dicoPlante.get(menuEspece.getSelectedIndex()).getConditionsOptimale().getTemperature());
        textqe=new JTextField(""+dicoPlante.get(menuEspece.getSelectedIndex()).getConditionsOptimale().getQuantiteEau());
        texte=new JTextField(""+dicoPlante.get(menuEspece.getSelectedIndex()).getEspece());
        texte.setColumns(5);
           
        cons.fill=GridBagConstraints.BOTH;
        cons.gridwidth=1; cons.gridx=0;cons.gridy=0;
        pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabModifEspece.png")),cons);
        cons.gridx++;
        pan.add(menuEspece,cons);
        cons.gridx=0;
        /////
        if(check){
            cons.gridwidth=1;
            cons.gridwidth=1;cons.gridx=0;cons.gridy++;
            pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabEspece.png")),cons);
            cons.gridx=1;
            pan.add(texte,cons);
            cons.gridy++;cons.gridx=0;
            pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabLuminosité.png")),cons);
            cons.gridx=1;
            pan.add(textl,cons);
            cons.gridy++;cons.gridx=0;
            pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabTempérature.png")),cons);
            cons.gridx=1;
            pan.add(textt,cons);
            cons.gridy++;cons.gridx=0;
            pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabHumidité.png")),cons);
            cons.gridx=1;
            pan.add(texth,cons);
            cons.gridy++;cons.gridx=0;
            pan.add(new JLabel(new ImageIcon("/home/pi/Bureau/B/LabQuantitéDeau.png")),cons);
            cons.gridx=1;
            pan.add(textqe,cons);
            cons.gridwidth=2;cons.gridx=0;cons.gridy++;
            ajouter.setBackground(Color.green);
            pan.add(ajouter,cons);
        }
        cons.gridy++;cons.gridwidth=2;
        retour.setBackground(Color.red);
        pan.add(retour,cons);
        
        this.setContentPane(pan);
        this.pack();

        menuEspece.addActionListener(this);
        retour.addActionListener(this);
        ajouter.addActionListener(this);
    }
  
    //ActionEvent
    /**
     * Gere le lien entre l'utilisation des boutons et le lien entre les pages de l'application
     * @param e l'evenement lié a l'utilisation des boutons
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        ///RAJOUTER UN ENUM DE MENU DANS LA CLASSE SCREEN
        //on pourra alors différentier les detections de boutton pour opti en temps
        //mais aussi réutiliser des boutons au lieux d'en avoir un milion en attribut
        String url = "jdbc:mysql://localhost:3306/ss2";
        String user = "user1";
        String mdp = "raspberry";
        
        if(focus==FocusMenu.Acceuil){//Focus sur acceuil
            if(e.getSource()==mesPlantes){
                try {
                    InitPlantes();
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(this,"Erreur de sauvegarde");
                }
            }
            if(e.getSource()==ajouterPlantes){
                if(plantes.getListePlante().size()==plantes.getPlantesMax())
                    JOptionPane.showMessageDialog(this,"Il n'y a plus de place pour ajouter une plante");
                else
                    InitAjouterPlante();
            }
            if(e.getSource()==gestionCapteurs){
                InitGestion();
            }
            if(e.getSource()==gestionEspece){
                InitIndexBD();
            }
            if(e.getSource()==parametre){
                InitParametre();
            }
        }
        
        if(focus==FocusMenu.Parametre){
            if(e.getSource()==retour){
                InitAcceuil();
            }
        }
        
        if(focus==FocusMenu.AjouterPlante){//Focus sur ajouter plante
            if(e.getSource()==retour){
                InitAcceuil();
            }
            if(e.getSource()==ajouter){
                plantes.addPlante(dicoPlante.get(menuEspece.getSelectedIndex()).getEspece(),dicoPlante.get(menuEspece.getSelectedIndex()).getConditionsActuelles().getQuantiteEau(), dicoPlante.get(menuEspece.getSelectedIndex()).getConditionsActuelles().getTemperature(), dicoPlante.get(menuEspece.getSelectedIndex()).getConditionsActuelles().getHumidite(), dicoPlante.get(menuEspece.getSelectedIndex()).getConditionsActuelles().getLuminosite());
                try {
                    InitPlantes();
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(this,"Erreur de sauvegarde");
                }
                JOptionPane.showMessageDialog(this,"La plante à été ajouté");  
            }
        }
        
        if(focus==FocusMenu.Gestion){ //Focus sur gestion
            if(e.getSource()==graph){
                InitGraphs();
            }
            if(e.getSource()==STOP){
                ///A FAIRE
            }
            if(e.getSource()==retour){
                InitAcceuil();
            }
        }
        
        if(focus==FocusMenu.Graphiques){//Focus sur graphique
            //AFAIRE
            if(e.getSource()==retour){
                InitGestion();
            }
        }
        
        if(focus==FocusMenu.Modif){ //Focus sur modif
            if(e.getSource()==retour){
                try {
                    InitPlantes();
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(this,"Erreur de sauvegarde");
                }
            }
            if(e.getSource()==ajouter){
                try{
                plantes.getListePlante().remove(focusPlante);
                
                plantes.addPlante(texte.getText(), Double.parseDouble(textqe.getText()), Double.parseDouble(textt.getText()), Double.parseDouble(texth.getText()), Double.parseDouble(textl.getText()));
                JOptionPane.showMessageDialog(this,"Les paramètres de la plantes ont été modifiés");
                try {
                    InitPlantes();
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(this,"Erreur de sauvegarde");
                }
                }
                catch(java.lang.NumberFormatException ex){
                    JOptionPane.showMessageDialog(this,"Champs invalides");
                    plantes.getListePlante().add(focusPlante);
                }
                focusPlante=null;
            }
        }
        
        if(focus==FocusMenu.MesPlantes){ //Focus sur Gestion des plantes
            if(e.getSource()==ajouterPlantes){
                if(plantes.getPlantesMax()==plantes.getListePlante().size())
                    JOptionPane.showMessageDialog(this,"Il n'y a plus de place pour ajouter une plante");
                else
                    InitAjouterPlante();
            }
            if(e.getSource()==retour){
                InitAcceuil();
            }
            for(int i=0;i<plantes.getListePlante().size();i++){
                if(tabB[i].equals(e.getSource())){//ici, on affiche les detail sous forme de JDialoque, cependant, un Jframe avec les detail et une possibilitée de modification est peu etre + adapté
                    JOptionPane.showMessageDialog(this,
                            "Quantité d'eau : "+plantes.getListePlante().get(i).getConditionsActuelles().getQuantiteEau()+"/"+plantes.getListePlante().get(i).getConditionsOptimale().getQuantiteEau()+
                            "\n Température : "+plantes.getListePlante().get(i).getConditionsActuelles().getTemperature()+"/"+plantes.getListePlante().get(i).getConditionsOptimale().getTemperature()+
                            "\n Humidité : "+plantes.getListePlante().get(i).getConditionsActuelles().getHumidite()+"/"+plantes.getListePlante().get(i).getConditionsOptimale().getHumidite()+
                            "\n Luminosité : "+plantes.getListePlante().get(i).getConditionsActuelles().getLuminosite()+"/"+plantes.getListePlante().get(i).getConditionsOptimale().getLuminosite());
                }
            } 
            for(int i=0;i<plantes.getListePlante().size();i++){
                if(tabSupp[i].equals(e.getSource())){
                    int rep = JOptionPane.showConfirmDialog(this ,"Voulez-vous supprimer cette plante?", "Gestion des plantes" , JOptionPane.YES_NO_OPTION);
                    if ( rep== JOptionPane. YES_OPTION){
                        plantes.delPlante(plantes.getListePlante().get(i));
                        //chosir le bon renvoie en menu
                try {
                    InitPlantes();
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(this,"Erreur de sauvegarde");
                }
                    }
                    if ( rep== JOptionPane. NO_OPTION){}//il se passe rien
                }
            }
            for(int i=0;i<plantes.getListePlante().size();i++){
                if(tabModif[i].equals(e.getSource())){
                    focusPlante=plantes.getListePlante().get(i);
                    InitModif();
                }
            }
        }
        if(focus==FocusMenu.IndexBD){//Focus sur Gestion des especes de plantes
            if(e.getSource()==explo){
                InitExploBD();
            }
            if(e.getSource()==add){
                InitaddBD();
            }
            if(e.getSource()==supp){
                InitsuppBD();
            }
            if(e.getSource()==modif){
                InitmodifBD();
            }
            if(e.getSource()==retour){
                InitAcceuil();
            }
        }
        if(focus==FocusMenu.AddBD){//Focus sur Gestion des especes de plantes
            if(e.getSource()==retour){
                InitIndexBD();
            }
             if(e.getSource()==ajouter){
                try{
                dicoPlante.add(new Plante(texte.getText(),new Condition(Double.parseDouble(textqe.getText()), Double.parseDouble(textt.getText()), Double.parseDouble(texth.getText()), Double.parseDouble(textl.getText())),new Condition(Double.parseDouble(textqe.getText()), Double.parseDouble(textt.getText()), Double.parseDouble(texth.getText()), Double.parseDouble(textl.getText()))));
                JOptionPane.showMessageDialog(this,"La plante à été ajoutée à la BD");
                InitIndexBD();
                //Connection cnx = DriverManager.getConnection("jdbc:sqlite:BD_SS2.sqlite");
                Connection cnx = DriverManager.getConnection(url, user, mdp);
                Statement link = cnx.createStatement();
                
                int value = link.executeUpdate("INSERT INTO PLANTES VALUES("+"'"+texte.getText()+"'"+","+"'"+textqe.getText()+"'"+","+"'"+textt.getText()+"'"+","+"'"+texth.getText()+"'"+")");
                }
                catch(java.lang.NumberFormatException ex){
                    JOptionPane.showMessageDialog(this,"Champs invalides");
                } catch (SQLException ex) {
                    Logger.getLogger(Screen.class.getName()).log(Level.SEVERE, null, ex);
                }
                InitIndexBD();
            }
        }
        if(focus==FocusMenu.SuppBD){//Focus sur Gestion des especes de plantes
            if(e.getSource()==supprimer){
                int rep=JOptionPane.showConfirmDialog(this, "Vouz-vous vraiment supprimer cette plante de la BD ?", null, JOptionPane.YES_NO_OPTION);
                if(rep==0) {
                    try {
                        //Connection cnx = DriverManager.getConnection("jdbc:sqlite:BD_SS2.sqlite");
                        Connection cnx = DriverManager.getConnection(url, user, mdp);
                        Statement link = cnx.createStatement();
                        
                        int values = link.executeUpdate("DELETE FROM PLANTES WHERE ESPECE = "+"'"+menuEspece.getSelectedItem()+"'");
                    } catch (SQLException ex) {
                        Logger.getLogger(Screen.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    dicoPlante.remove(dicoPlante.get(menuEspece.getSelectedIndex()));
                    JOptionPane.showMessageDialog(this,"La plante à été supprimer de la base de données");
                }
                InitsuppBD();
            }
            if(e.getSource()==retour){
                InitIndexBD();
            }
        }
        if(focus==FocusMenu.ModifBD){//Focus sur Gestion des especes de plantes
            if(e.getSource()==retour){
                check=false;
                InitIndexBD();
            }
            if(e.getSource()==menuEspece){
                check=true;
                InitmodifBD();
            }
            if(e.getSource()==ajouter){
                try{
                focusPlante=dicoPlante.get(menuEspece.getSelectedIndex());
               
                dicoPlante.remove(focusPlante);
                
                dicoPlante.add(new Plante(texte.getText(),new Condition( Double.parseDouble(textqe.getText()), Double.parseDouble(textt.getText()), Double.parseDouble(texth.getText()), Double.parseDouble(textl.getText())),new Condition( Double.parseDouble(textqe.getText()), Double.parseDouble(textt.getText()), Double.parseDouble(texth.getText()), Double.parseDouble(textl.getText()))));
                JOptionPane.showMessageDialog(this,"La plante à été modifiée dans la BD");
                 try {
                        //Connection cnx = DriverManager.getConnection("jdbc:sqlite:BD_SS2.sqlite");
                        Connection cnx = DriverManager.getConnection(url, user, mdp);
                        Statement link = cnx.createStatement();
                        
                        int values = link.executeUpdate("DELETE FROM PLANTES WHERE ESPECE = "+"'"+menuEspece.getSelectedItem()+"'");
                        values = link.executeUpdate("INSERT INTO PLANTES VALUES("+"'"+texte.getText()+"'"+","+"'"+textqe.getText()+"'"+","+"'"+textt.getText()+"'"+","+"'"+texth.getText()+"'"+")");
                    } catch (SQLException ex) {
                        Logger.getLogger(Screen.class.getName()).log(Level.SEVERE, null, ex);
                    }
                InitIndexBD();
                }
                catch(java.lang.NumberFormatException ex){
                    JOptionPane.showMessageDialog(this,"Champs invalides");   
                    dicoPlante.add(focusPlante);
                }
                focusPlante=null;
                check=false;
            }
        }
        if(focus==FocusMenu.ExploBD){
            if(e.getSource()==retour){
                InitIndexBD();
                check=false;
            }
            if(e.getSource()==menuEspece){
                check=true;
                InitExploBD();
            }
        }
    } 
    
    //méthodes diverses
    /**
     * Fonction permettant de lire la base de données
     * @throws ClassNotFoundException si on ne trouve pas la classe (erreur de lecture/d'ecriture)
     * @throws SQLException  Si on ne trouve pas la base de donnée (erreur de lecture/d'ecriture)
     */
    public void LoadDico() throws ClassNotFoundException, SQLException{
        dicoPlante=new ArrayList<Plante>();
        String url = "jdbc:mysql://localhost:3306/ss2";
        String user = "user1";
        String mdp = "raspberry";
        //include la lecture de fichier
        try {
            // TODO code application logic here
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            //Connection cnx = DriverManager.getConnection("jdbc:sqlite:/home/pi/Bureau/ss2/PROJET/Irrigation/dist/BD_SS2.sqlite");
            //Connection cnx = DriverManager.getConnection("jdbc:sqlite:BD_SS2.sqlite");
            Connection cnx = DriverManager.getConnection(url, user, mdp);
            Statement link = cnx.createStatement();
            
            ResultSet rs = link.executeQuery("SELECT * FROM PLANTES");
            while(rs.next()){
                dicoPlante.add(new Plante(rs.getString("ESPECE"),new Condition(rs.getDouble("QUANTITE_EAU"),rs.getDouble("TEMPERATURE"), rs.getDouble("HUMIDITE"),1),new Condition(1,1,1,1)));
            }
        } catch (InstantiationException ex) {
            Logger.getLogger(Irrigation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Irrigation.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //en attendant :
    }
    

    public void setPlantes(Plantes plantes) {
        this.plantes = plantes;
    } 

    public JLabel getValueHum() {
        return valueHum;
    }

    public void setValueHum(JLabel valueHum) {
        this.valueHum = valueHum;
    }

    public JLabel getValueLum() {
        return valueLum;
    }

    public void setValueLum(JLabel valueLum) {
        this.valueLum = valueLum;
    }

    public JLabel getValueTemp() {
        return valueTemp;
    }

    public void setValueTemp(JLabel valueTemp) {
        this.valueTemp = valueTemp;
    }

    public Plantes getPlantes() {
        return plantes;
    }
    
    /**
     * Fonction qui sert a garder les plantes actuelles dans un fichier texte.
     * @throws FileNotFoundException Si on ne trouve pas le fichier
     */
    public void save() throws FileNotFoundException{
        PrintWriter writer;
        writer= new PrintWriter("/home/pi/Bureau/plante.txt");
        for(int j=0;j<plantes.getListePlante().size();j++){
            writer.println(""+plantes.getListePlante().get(j).getEspece());
            writer.println(""+plantes.getListePlante().get(j).getConditionsOptimale().getQuantiteEau());
            writer.println(""+plantes.getListePlante().get(j).getConditionsOptimale().getTemperature());
            writer.println(""+plantes.getListePlante().get(j).getConditionsOptimale().getHumidite());
            writer.println(""+plantes.getListePlante().get(j).getConditionsOptimale().getLuminosite());
        }
        writer.close();
    }
    
    public void popUp(String a){
        JOptionPane.showMessageDialog(this,a);
    }
}
