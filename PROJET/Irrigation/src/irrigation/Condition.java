/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package irrigation;

/**
 * C'est la classe qui définit les conditions optimales de chaque plantes
 * @author Virgile Gabrieli
 * @version 2.0
 */
public class Condition {
    private double quantiteEau;
    private double temperature;
    private double humidite;
    private double luminosite;

    /**
     * Constructeur de la classe
     * @param quantiteEau la quantité d'eau optimale pour la croissance de la plante
     * @param temperature la temperature optimale pour la croissance de la plante
     * @param humidite le taux d'humidité optimal pour la croissance de la plante
     * @param luminosite la luminosité optimale pour la croissance de la plante
     */
    public Condition(double quantiteEau, double temperature, double humidite, double luminosite) {
        this.quantiteEau = quantiteEau;
        this.temperature = temperature;
        this.humidite = humidite;
        this.luminosite = luminosite;
    }

    
    public double getQuantiteEau() {
        return quantiteEau;
    }
    
    /**
     * Cette fonction sert a changer la quantité d'eau optimale d'une plante si l'utilisateur la change manuellement depuis la base de donnée
     * @param quantiteEau l'entrée de l'utilisateur
     */
    public void setQuantiteEau(double quantiteEau) {
        this.quantiteEau = quantiteEau;
    }
    
    public double getTemperature() {
        return temperature;
    }
     /**
     * Cette fonction sert a changer la temperature optimale d'une plante si l'utilisateur la change manuellement depuis la base de donnée
     * @param temperature l'entrée de l'utilisateur
     */
    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getHumidite() {
        return humidite;
    }
    /**
     * Cette fonction sert a changer l'humidite optimale d'une plante si l'utilisateur la change manuellement depuis la base de donnée
     * @param humidite l'entrée de l'utilisateur
     */
    public void setHumidite(double humidite) {
        this.humidite = humidite;
    }

    public double getLuminosite() {
        return luminosite;
    }
    /**
     * Cette fonction sert a changer la luminosite optimale d'une plante si l'utilisateur la change manuellement depuis la base de donnée
     * @param luminosite l'entrée de l'utilisateur
     */
    public void setLuminosite(double luminosite) {
        this.luminosite = luminosite;
    }
    
    
}
