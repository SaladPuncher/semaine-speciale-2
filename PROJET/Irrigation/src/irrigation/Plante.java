/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package irrigation;

/**
 * C'est la classe qui cree les plantes.
 * @author Virgile Gabrieli
 */
public class Plante {
    private String espece;
    private Condition conditionsOptimale;
    private Condition conditionsActuelles;
    
    /**
     * Constructeur de la classe
     * @param espece L'espece de plante
     * @param conditionsOptimale Les conditions optimales de la plante
     * @param conditionsActuelles Les conditions actuelles de la plante
     */
    public Plante(String espece, Condition conditionsOptimale, Condition conditionsActuelles) {
        this.espece = espece;
        this.conditionsOptimale = conditionsOptimale;
        this.conditionsActuelles = conditionsActuelles;
    }
    /**
     * Costructeur de la classe, sans les conditions actuelles, qui peuvent être récupérées plus tard
     * @param espece L'espece de la plante
     * @param conditionsOptimale Les conditions optimales de la plante
     */
    public Plante(String espece, Condition conditionsOptimale) {
        this.espece = espece;
        this.conditionsOptimale = conditionsOptimale;
    }
    
    
    public Plante(String espece){ //a enlever, cst pour des tests
        this.espece=espece;
    }
    
    public String getEspece() {
        return espece;
    }
    
    public void setEspece(String espece) {
        this.espece = espece;
    }
    
    public Condition getConditionsOptimale() {
        return conditionsOptimale;
    }
    
    public void setConditionsOptimale(Condition conditionsOptimale) {
        this.conditionsOptimale = conditionsOptimale;
    }
    
    public Condition getConditionsActuelles() {
        return conditionsActuelles;
    }
    
    public void setConditionsActuelles(Condition conditionsActuelles) {
        this.conditionsActuelles = conditionsActuelles;
    }
    
    
}
