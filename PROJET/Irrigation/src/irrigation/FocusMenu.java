/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package irrigation;

/**
 * Permet, lorsque l'utilisateur change de menu dans l'application, d'afficher les bon bouttons en evitant les erreurs quelconques.
 * @version 1.0
 * @author Virgile Gabrieli
 */

public enum FocusMenu {
    Acceuil,
    MesPlantes,
    AjouterPlante,
    Gestion,
    Graphiques,
    Modif,
    IndexBD,
    AddBD,
    SuppBD,
    ModifBD,
    ExploBD,
    Parametre;
}
