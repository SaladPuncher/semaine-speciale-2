/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package irrigation;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;

/**
 * Surcharge de la classe JPanel, permettant d'afficher une image dans une JPanel.
 * @version 2.0
 * @author Virgile Gabrieli
 */
public class ImagePanel extends JPanel{
    
    private static final long serialVersionUID = 1L;
    private Image image = null;
    private int iWidth2;
    private int iHeight2;
    
    /**
     * Constructeur de la classe.
     * @param image l'imaga a afficher
     */
    public ImagePanel(Image image)
    {
        this.image = image;
        this.iWidth2 = image.getWidth(this)/2;
        this.iHeight2 = image.getHeight(this)/2;
    }
    /**
     * Sert a changer l'imaga a afficher.
     * @param image l'image a afficher
     */
    public void setImage(Image image) {
        this.image = image;
        this.iWidth2 = image.getWidth(this)/2;
        this.iHeight2 = image.getHeight(this)/2;
    }
    
    /**
     * Permet d'afficher l'imgage dans le JPanel.
     * @param g 
     */
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        if (image != null)
        {
            int x = this.getParent().getWidth()/2 - iWidth2;
            int y = this.getParent().getHeight()/2 - iHeight2;
            g.drawImage(image,x,y,this);
        }
    }
}