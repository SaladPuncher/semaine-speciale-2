/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package irrigation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * C'est la classe qui sert a prendre les données du capteur humidité et température
 * @author Ryan Murati
 * @version 1.0
 */

public class CapteurTempHumid {
    public ArrayList<Double> listeTemp = new ArrayList();
    public ArrayList<Double> listeHumid = new ArrayList();
    public ArrayList<Double> listeSoil = new ArrayList();
    
    /**
     * Cette fonction sert a lire les données des capteurs
     * @param file le fichier texte dans lequel se trouve les données
     * @throws Exception Si chemin du dossier n'est pas le bon
     */
    public void loadFile(String file) throws Exception{
        
        listeTemp = new ArrayList();
        listeHumid = new ArrayList();
        listeSoil = new ArrayList();
        
        File fich = new File(file);
        Scanner scan;
        try{
            scan = new Scanner(fich);
        }
        catch (FileNotFoundException f){
            System.out.println("c raté");
            throw new Exception("Mauvais chemin");
        }
        while (scan.hasNext()){
            System.out.println("sacn");
            listeTemp.add(Double.parseDouble(scan.next()));
            listeHumid.add(Double.parseDouble(scan.next()));
            listeSoil.add(Double.parseDouble(scan.next()));
        }
        
    }

    public ArrayList<Double> getListeSoil() {
        return listeSoil;
    }

    public ArrayList<Double> getListeTemp() {
        return listeTemp;
    }

    public ArrayList<Double> getListeHumid() {
        return listeHumid;
    }

    public CapteurTempHumid() {
        
    }
    /**
     * Cette fonction sert a get les données de l'humidité du capteur
     */
    public void getListeHumidAff() {
        for (int i=0; i<this.listeHumid.size(); i++){
            System.out.println("h"+this.listeHumid.get(i));
        }
    }
    /**
     * Cette fonction sert a get les données de la temperature du capteur
     */
    public void getListeTempAff() {
        for (int i=0; i<this.listeTemp.size(); i++){
            System.out.println("t"+this.listeTemp.get(i));
        }
    }
    
    
    
}
