/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package irrigation;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JComponent;

/**
 * Classe qui contient les plantes qu'à l'utilisateur.
 * @author Virgile Gabrieli
 */
public class Plantes{
    private ArrayList<Plante> listePlante;
    private int plantesMax;
    
    /**
     * Constructeur de la classe.
     */
    Plantes(){
        listePlante = new ArrayList<Plante>();
        plantesMax=4;
    }
    
    /**
     * Fonction qui permet d'ajouter une plante à la liste de plante
     * @param p la plante a ajouter
     */
    void addPlante(Plante p){//il faudra surement modifier la fonction en creant direct la plante au lieu de la recevoir
        if(listePlante.size()<plantesMax)
            listePlante.add(p);
        //faut rajouter un else en cas derreur ou un trow execption
    }
    /**
     * /**
     * Fonction qui permet d'ajouter une plante à la liste de plante en créant une nouvelle plante
     * @param espece l'espece de la plante
     * @param Qe la quantité d'eau optimale de la plante
     * @param temp la temperature optimale de la plante
     * @param humi le taux d'humidité optimale de la plante
     * @param lumi la luminosité optimal de la plante
     */
    void addPlante(String espece,double Qe, double temp, double humi,double lumi){
        if(listePlante.size()<plantesMax)//check la
            listePlante.add(new Plante(espece,new Condition(Qe,temp,humi,lumi),new Condition(Qe,temp,humi,lumi)));
    }
    
    /**
     * Fonction servant a supprimer une plante de la liste de plante
     * @param p la plante à supprimer
     */
    void delPlante(Plante p){
        listePlante.remove(p);
    }

    public int getPlantesMax() {
        return plantesMax;
    }

    public void setPlantesMax(int plantesMax) {
        this.plantesMax = plantesMax;
    }

    
    
    public ArrayList<Plante> getListePlante() {
        return listePlante;
    }

    public void setListePlante(ArrayList<Plante> listePlante) {
        this.listePlante = listePlante;
    }
    
    
    
}
