/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package irrigation;

import java.io.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.python.util.PythonInterpreter;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 *
 * @author p1807500
 */
public class Irrigation {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException, FileNotFoundException,java.io.IOException {
        Screen f= new Screen();
        try{
            /*
            Plantes plantes= new Plantes();
            Condition conditionActuelle = new Condition(1,1,1,1);
            Condition conditionOptimales = new Condition(2,1000,2,2);
            Plante p = new Plante("Tulipe",conditionActuelle,conditionOptimales);
            Plante p1 = new Plante("Rose",conditionActuelle,conditionOptimales);
            Plante p2 = new Plante("Marguerite",conditionActuelle,conditionOptimales);
            plantes.addPlante(p);
            plantes.addPlante(p1);
            plantes.addPlante(p2);
            */
            java.util.Scanner lecteur ;
            java.io.File fichier = new java.io.File("/home/pi/Bureau/plante.txt");//a changer
            lecteur = new java.util.Scanner(fichier);
            Plantes plantes=new Plantes();
            try{
                while (lecteur.hasNext()){
                    String s=lecteur.next();
                    Double d1=Double.parseDouble(lecteur.next());
                    Double d2=Double.parseDouble(lecteur.next());
                    Double d3=Double.parseDouble(lecteur.next());
                    Double d4=Double.parseDouble(lecteur.next());
                    Plante p=new Plante(s,new Condition(d1,d2,d3,d4),new Condition(-1,-1,-1,-1));
                    plantes.addPlante(p);
                }
            }
            catch(Exception e){
                System.out.println("bog");
                //jdialogue a mettre ici
            }
            
            f.setPlantes(plantes);
            
            String cmd ; int i=0;
            CapteurTempHumid capt = new CapteurTempHumid();
            capt.loadFile("/home/pi/Bureau/ss2/PROJET/history.txt");
            while(true){
                f.getValueHum().setText(""+capt.getListeSoil().get(i));
                f.getValueTemp().setText(""+capt.getListeTemp().get(i));
                if(
                        capt.getListeTemp().get(i)
                        >
                        plantes.getListePlante().get(0).getConditionsOptimale().getTemperature()
                        ){
                    System.out.println("a");
                    f.popUp("Trop chaud");
                    
                }
                
                if(capt.getListeTemp().get(i)<plantes.getListePlante().get(0).getConditionsOptimale().getTemperature()){
                    System.out.println("b");
                    f.popUp("Trop froid");
                }
                if(capt.getListeSoil().get(i)<plantes.getListePlante().get(0).getConditionsOptimale().getHumidite()){
                    System.out.println("c");
                    f.popUp("Il faut arroser votre plante");
                }
                System.out.println("4");
                i++;
                try {
                    Thread.sleep(1000*60) ;
                }  catch (InterruptedException e) {
                    // gestion de l'erreur
                }
            }
            
        }
        catch(Exception e){
        }
        //lecture
        /*
        java.util.Scanner lecteur ;
        java.io.File fichier = new java.io.File("C:\\Users\\virgi\\Desktop\\test.txt");//a changer
        lecteur = new java.util.Scanner(fichier);
        Plantes plantation=new Plantes();
        try{
        while (lecteur.hasNext()){
        String s=lecteur.next();
        Double d1=Double.parseDouble(lecteur.next());
        Double d2=Double.parseDouble(lecteur.next());
        Double d3=Double.parseDouble(lecteur.next());
        Double d4=Double.parseDouble(lecteur.next());
        Plante p=new Plante(s,new Condition(d1,d2,d3,d4),new Condition(-1,-1,-1,-1));
        plantation.addPlante(p);
        }
        }
        catch(Exception e){
        System.out.println("bog");
        //jdialogue a mettre ici
        }
        */
        //ecriture
        /*
        PrintWriter writer = new PrintWriter("C:\\Users\\virgi\\Desktop\\test.txt");
        for(int i=0;i<plantation.getListePlante().size();i++){
        writer.println(""+plantation.getListePlante().get(i).getEspece());
        writer.println(""+plantation.getListePlante().get(i).getConditionsOptimale().getQuantiteEau());
        writer.println(""+plantation.getListePlante().get(i).getConditionsOptimale().getTemperature());
        writer.println(""+plantation.getListePlante().get(i).getConditionsOptimale().getHumidite());
        writer.println(""+plantation.getListePlante().get(i).getConditionsOptimale().getLuminosite());
        }
        writer.close();
        */
    }
    
}
